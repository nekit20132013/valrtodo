import {configureStore} from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import taskSlice from './reducers/taskSlice';
import taskSaga from './sagas/taskSaga';

let sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    task: taskSlice.reducer,
  },
  middleware: getDefaultMiddleware => [
    ...getDefaultMiddleware(),
    sagaMiddleware,
  ],
});

sagaMiddleware.run(taskSaga);

export default store;
