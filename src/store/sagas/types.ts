import {TasksI} from '@store/reducers/types';

export interface ChangeTaskStatusSagaPayload {
  payload: {taskId: string; taskStatus: boolean};
}

export interface DeleteTaskSagaPayload {
  taskId: string;
}

export interface TaskSagaPayload {
  task: TasksI;
}
