import {takeEvery, put, call, all} from 'redux-saga/effects';

import {TASK_SAGA_ACTIONS} from '@constants/actions';
import {
  fetchTasks,
  setTask,
  deleteTask,
  changeTaskStatus,
} from '../reducers/taskSlice';
import Api from '@api';
import {
  TaskSagaPayload,
  DeleteTaskSagaPayload,
  ChangeTaskStatusSagaPayload,
} from './types';

export function* fetchTasksSaga() {
  try {
    const result = yield call(Api.task.getTaskList);
    yield put(fetchTasks(result));
  } catch {
    console.log('fetchTasksSaga error');
  }
}

export function* setTaskSaga({task}: TaskSagaPayload) {
  try {
    const result = yield call(Api.task.setTask, task);
    if (result) {
      yield put(setTask(task));
    }
  } catch {
    console.log('setTaskSaga error');
  }
}

export function* deleteTaskSaga({taskId}: DeleteTaskSagaPayload) {
  try {
    const result = yield call(Api.task.deleteTask, taskId);
    if (result) {
      yield put(deleteTask({taskId}));
    }
  } catch {
    console.log('deleteTaskSaga error');
  }
}

export function* changeTaskStatusSaga({
  payload: {taskId, taskStatus},
}: ChangeTaskStatusSagaPayload) {
  try {
    const result = yield call(Api.task.changeTaskStatus, taskId, taskStatus);
    if (result) {
      yield put(changeTaskStatus({taskId, taskStatus}));
    }
  } catch {
    console.log('changeTaskStatusSaga error');
  }
}

export default function* taskSaga() {
  yield all([
    takeEvery(TASK_SAGA_ACTIONS.FETCH_TASKS, fetchTasksSaga),
    takeEvery(TASK_SAGA_ACTIONS.SET_TASK, setTaskSaga),
    takeEvery(TASK_SAGA_ACTIONS.DELETE_TASK, deleteTaskSaga),
    takeEvery(TASK_SAGA_ACTIONS.CHANGE_TASK_STATUS, changeTaskStatusSaga),
  ]);
}
