export interface TaskI {
  id: string;
  name: string;
  isCompleted: boolean;
}

export interface TasksI {
  tasks: Array<TaskI>;
}

export interface StateI {
  task: TasksI;
}

export interface ActionPayloadSetTaskI {
  type: string;
  payload: TaskI;
}

export interface ActionPayloadDeleteTaskI {
  type: string;
  payload: {
    taskId: string;
  };
}

export interface ActionPayloadChangeTaskStatusI {
  type: string;
  payload: {
    taskId: string;
    taskStatus: boolean;
  };
}
