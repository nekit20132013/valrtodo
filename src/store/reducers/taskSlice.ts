import {createSlice} from '@reduxjs/toolkit';

import {
  ActionPayloadSetTaskI,
  ActionPayloadDeleteTaskI,
  ActionPayloadChangeTaskStatusI,
  TaskI,
} from './types';

const taskSlice = createSlice({
  name: 'task',
  initialState: {
    tasks: [],
  },
  reducers: {
    fetchTasks: (state, action) => {
      return {
        ...state,
        tasks: action.payload,
      };
    },
    setTask: (state, action: ActionPayloadSetTaskI) => {
      const tasks: Array<TaskI> = [...state.tasks, action.payload];
      return {
        ...state,
        tasks,
      };
    },
    deleteTask: (state, action: ActionPayloadDeleteTaskI) => {
      const indexTask: number = state.tasks.findIndex(
        ({id}) => id === action.payload.taskId,
      );
      let tasks = [...state.tasks];
      tasks.splice(indexTask, 1);
      return {
        ...state,
        tasks,
      };
    },
    changeTaskStatus: (state, action: ActionPayloadChangeTaskStatusI) => {
      return {
        ...state,
        tasks: state.tasks.map((item: TaskI) => {
          if (item.id === action.payload.taskId) {
            return {
              ...item,
              isCompleted: action.payload.taskStatus,
            };
          }
          return item;
        }),
      };
    },
  },
});

export default taskSlice;

export const {fetchTasks, setTask, deleteTask, changeTaskStatus} =
  taskSlice.actions;
