import task from './endpoints/task';

const Api = {
  task: task(),
};

export default Api;
