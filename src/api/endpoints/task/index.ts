import {DELAY_TIME_MS} from '@constants/api';

import {TASK_KEYS} from '@constants/asyncStorage';
import {TaskI} from '@store/reducers/types';
import {getItemFromStorage, setItemToStorage} from '@helpers/asyncStorage';

//Database simulation and back-end requests

export default () => ({
  setTask(task: TaskI): Promise<boolean> {
    return new Promise(async resolve => {
      let tasks = await getItemFromStorage(TASK_KEYS.TASKS);
      tasks.push(task);
      await setItemToStorage(TASK_KEYS.TASKS, tasks);
      setTimeout(() => resolve(true), DELAY_TIME_MS);
    });
  },
  getTaskList() {
    return new Promise(async resolve => {
      const tasks = await getItemFromStorage(TASK_KEYS.TASKS);
      setTimeout(() => resolve(tasks), DELAY_TIME_MS);
    });
  },
  deleteTask(taskId: string): Promise<boolean> {
    return new Promise(async resolve => {
      let tasks = await getItemFromStorage(TASK_KEYS.TASKS);
      tasks = tasks.filter(({id}: TaskI) => id !== taskId);
      await setItemToStorage(TASK_KEYS.TASKS, tasks);
      setTimeout(() => resolve(true), DELAY_TIME_MS);
    });
  },
  changeTaskStatus(taskId: string, taskStatus: boolean): Promise<boolean> {
    return new Promise(async resolve => {
      let tasks = await getItemFromStorage(TASK_KEYS.TASKS);
      tasks = tasks.map((item: TaskI) => {
        if (item?.id === taskId) {
          return {...item, isCompleted: taskStatus};
        }
        return item;
      });
      await setItemToStorage(TASK_KEYS.TASKS, tasks);
      setTimeout(() => resolve(true), DELAY_TIME_MS);
    });
  },
});
