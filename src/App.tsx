import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';

import 'react-native-get-random-values';

import store from './store/store';
import Navigation from './navigation';

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor="transparent"
        translucent
      />
      <Navigation />
    </Provider>
  );
};

export default App;
