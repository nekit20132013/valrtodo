import React from 'react';
import {styles} from './styles';
import {Text, View} from 'react-native';

export const TaskListEmpty = () => {
  return (
    <View style={styles.listNone}>
      <View style={styles.notListContainer}>
        <Text style={styles.notListTextBold}>The task list is empty</Text>
        <Text style={styles.notListText}>Create new task</Text>
      </View>
    </View>
  );
};
