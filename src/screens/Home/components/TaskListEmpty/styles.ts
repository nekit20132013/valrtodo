import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  listNone: {
    borderTopColor: COLORS.gray[500],
    borderTopWidth: 1,

    height: 208,
    width: '100%',

    justifyContent: 'center',
    alignItems: 'center',
  },
  notListContainer: {
    marginTop: 16,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  notListTextBold: {
    textAlign: 'center',
    color: COLORS.gray[300],
  },
  notListText: {
    textAlign: 'center',
    color: COLORS.gray[300],
  },
});
