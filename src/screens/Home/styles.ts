import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.gray[200],
  },
  form: {
    width: '100%',
    height: 54,
    flexDirection: 'row',
    paddingHorizontal: 24,
  },
  listWrapper: {
    width: '100%',
    minHeight: 327,
    marginTop: 48,
    paddingHorizontal: 24,
  },
  counters: {
    width: '100%',
    height: 19,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: 20,
  },
});
