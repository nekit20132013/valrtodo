import React from 'react';
import {Alert, FlatList, View, SafeAreaView} from 'react-native';
import {v4 as uuidv4} from 'uuid';

import {Button} from '@components/Button';
import {Input} from '@components/Input';
import {Task} from '@components/Task';
import {TaskListEmpty} from './components/TaskListEmpty';
import {Counter} from '@components/Counter';
import {useTasks} from '@hooks';

import {styles} from './styles';

const Home = () => {
  const {
    name,
    setName,
    tasks,
    createTask,
    updateTaskStatus,
    deleteTask,
    counterCompletedList,
  } = useTasks();

  function handleItemAdd() {
    const task = {
      id: uuidv4(),
      name: name,
      isCompleted: false,
    };
    createTask(task);
    setName('');
  }

  function handleToggleCompletedById(taskId: string, taskStatus: boolean) {
    updateTaskStatus({taskId, taskStatus});
  }

  function handleTaskRemove(taskId: string, description: string) {
    Alert.alert('Remover', `Remove the task '${description}'?`, [
      {
        text: 'Yes',
        onPress: () => deleteTask(taskId),
      },
      {
        text: 'No',
        style: 'cancel',
      },
    ]);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.form}>
        <Input
          placeholder="Input the task title"
          onChangeText={setName}
          value={name}
        />
        <Button onPress={handleItemAdd} label={'Add'} />
      </View>

      <View style={styles.listWrapper}>
        <View style={styles.counters}>
          <Counter count={tasks.length} label={'Created'} />
          <Counter count={counterCompletedList} label={'Completed'} />
        </View>

        <FlatList
          data={tasks}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <Task
              key={item?.id}
              name={item?.name}
              id={item?.id}
              isChecked={item?.isCompleted}
              onCompleted={handleToggleCompletedById}
              onRemove={() => handleTaskRemove(item?.id, item?.name)}
            />
          )}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={TaskListEmpty}
        />
      </View>
    </SafeAreaView>
  );
};

export default Home;
