import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  divider: {
    borderTopColor: COLORS.gray[300],
    borderBottomWidth: 1,
  },
});
