import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  default: {
    width: 52,
    height: 52,
    borderRadius: 5,
    backgroundColor: COLORS.blue.dark,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelDefault: {
    color: '#fff',
    fontSize: 14,
  },
  ghost: {
    color: COLORS.blue.dark,
  },
  labelGhost: {
    color: COLORS.blue.dark,
  },
});
