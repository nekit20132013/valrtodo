export enum VariantButton {
  ghost = 'ghost',
}

export interface ButtonProps {
  onPress: () => void;
  label: string;
  variant?: 'ghost';
}
