import React, {useMemo} from 'react';
import {TouchableOpacity, Text} from 'react-native';

import {ButtonProps, VariantButton} from './types';

import {styles} from './styles';

export function Button({onPress, label, variant}: ButtonProps) {
  const style = useMemo(() => {
    switch (variant) {
      case VariantButton.ghost:
        return {container: styles.ghost, label: styles.labelGhost};
      default:
        return {container: styles.default, label: styles.labelDefault};
    }
  }, [variant]);
  return (
    <TouchableOpacity style={{...style.container}} onPress={onPress}>
      <Text style={{...styles.labelDefault, ...style.label}}>{label}</Text>
    </TouchableOpacity>
  );
}
