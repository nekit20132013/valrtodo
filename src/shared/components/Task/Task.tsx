import {View} from 'react-native';

import {Checkbox} from '@components/Checkbox';
import {Button} from '@components/Button';

import {TaskProps} from './types';
import {VariantButton} from '@components/Button/types';

import {styles} from './styles';

export function Task({id, name, onRemove, onCompleted, isChecked}: TaskProps) {
  function handleChangeChecked() {
    onCompleted(id, !isChecked);
  }

  return (
    <View style={styles.container}>
      <Checkbox
        text={name}
        checked={isChecked}
        onChecked={handleChangeChecked}
      />
      <Button
        onPress={onRemove}
        label={'delete'}
        variant={VariantButton.ghost}
      />
    </View>
  );
}
