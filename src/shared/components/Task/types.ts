export interface TaskProps {
  id: string;
  name: string;
  isChecked: boolean;
  onRemove: () => void;
  onCompleted: (taskId: string, taskStatus: boolean) => void;
}
