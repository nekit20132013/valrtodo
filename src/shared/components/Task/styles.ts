import {COLORS} from '@constants/colors';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 64,

    backgroundColor: COLORS.gray[100],
    borderRadius: 8,

    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

    gap: 8,
    marginBottom: 8,
    paddingHorizontal: 12,
    paddingStart: 12,
    paddingEnd: 8,
  },
  name: {
    flex: 1,
    color: COLORS.gray[700],
    marginLeft: 16,
  },
});
