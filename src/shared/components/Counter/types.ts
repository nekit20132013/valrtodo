export interface CounterProps {
  count: number;
  label: string;
}
