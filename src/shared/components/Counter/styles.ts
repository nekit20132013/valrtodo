import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  counterItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 19,
  },
  counterWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 19,
    height: 19,
    backgroundColor: COLORS.blue.dark,
    borderRadius: 999,
    marginLeft: 10,
  },
  counter: {
    color: '#fff',
    fontSize: 12,
  },
  label: {
    fontSize: 15,
  },
});
