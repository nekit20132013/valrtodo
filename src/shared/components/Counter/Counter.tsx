import React from 'react';
import {Text, View} from 'react-native';

import {CounterProps} from './types';

import {styles} from './styles';

export const Counter = ({count, label}: CounterProps) => {
  return (
    <View style={styles.counterItem}>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.counterWrapper}>
        <Text style={styles.counter}>{count}</Text>
      </View>
    </View>
  );
};
