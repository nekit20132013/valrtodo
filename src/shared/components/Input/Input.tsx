import {TextInput} from 'react-native';
import {styles} from './styles';
import {COLORS} from '@constants/colors';

export const Input = ({...props}) => {
  return (
    <TextInput
      style={styles.input}
      keyboardAppearance="dark"
      placeholderTextColor={COLORS.gray[700]}
      {...props}
    />
  );
};
