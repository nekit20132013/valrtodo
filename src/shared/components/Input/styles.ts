import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  input: {
    flex: 1,
    marginEnd: 8,
    padding: 16,
    gap: 8,
    height: 54,
    fontSize: 16,
    color: COLORS.gray[500],

    backgroundColor: COLORS.gray[100],
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: COLORS.gray[100],

    borderRadius: 8,

    focused: {
      border: ` ${1} solid ${COLORS.danger}`,
    },
  },
});
