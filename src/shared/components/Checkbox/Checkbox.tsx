import {Pressable, Text} from 'react-native';

import {CheckboxProps} from './types';

import {styles} from './styles';

export function Checkbox({text, checked, onChecked, ...props}: CheckboxProps) {
  return (
    <>
      <Pressable
        style={[styles.checkboxBase, checked && styles.checkboxChecked]}
        onPress={onChecked}
        {...props}
      />
      <Text style={[styles.text, checked && styles.textChecked]}>{text}</Text>
    </>
  );
}
