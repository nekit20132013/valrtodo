export interface CheckboxProps {
  text?: string;
  checked: boolean;
  onChecked: () => void;
}
