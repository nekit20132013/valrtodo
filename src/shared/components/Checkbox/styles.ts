import {StyleSheet} from 'react-native';

import {COLORS} from '@constants/colors';

export const styles = StyleSheet.create({
  checkboxBase: {
    width: 17.45,
    height: 17.45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 999,
    borderWidth: 2,
    borderColor: COLORS.blue.default,
    backgroundColor: 'transparent',
    margin: 3.27,
  },
  checkboxChecked: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.purple.default,
    borderColor: COLORS.purple.default,
  },
  text: {
    flex: 1,
    width: 235,
    color: COLORS.gray[700],
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  textChecked: {
    color: COLORS.gray[300],
    textDecorationLine: 'line-through',
  },
});
