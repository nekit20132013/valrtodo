import {useDispatch, useSelector} from 'react-redux';
import {useEffect, useMemo, useState} from 'react';
import {TASK_SAGA_ACTIONS} from '@constants/actions';
import {StateI, TaskI} from '@store/reducers/types';

interface UpdateTaskStatusPayloadI {
  taskId: string;
  taskStatus: boolean;
}

export const useTasks = () => {
  const dispatch = useDispatch();
  const tasks = useSelector((state: StateI) => state.task.tasks);
  const [name, setName] = useState('');

  useEffect(() => {
    dispatch({type: TASK_SAGA_ACTIONS.FETCH_TASKS});
  }, []);

  const counterCompletedList = useMemo(
    () => tasks.filter(task => task.isCompleted).length,
    [tasks],
  );

  const createTask = (task: TaskI) =>
    dispatch({type: TASK_SAGA_ACTIONS.SET_TASK, task});

  const updateTaskStatus = (payload: UpdateTaskStatusPayloadI) =>
    dispatch({type: TASK_SAGA_ACTIONS.CHANGE_TASK_STATUS, payload});

  const deleteTask = (taskId: string) =>
    dispatch({type: TASK_SAGA_ACTIONS.DELETE_TASK, taskId});

  return {
    name,
    setName,
    tasks,
    createTask,
    updateTaskStatus,
    deleteTask,
    counterCompletedList,
  };
};
