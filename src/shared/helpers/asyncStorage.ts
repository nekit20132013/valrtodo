import AsyncStorage from '@react-native-async-storage/async-storage';

export const getItemFromStorage = async (
  key: string,
  defaultValue: unknown = [],
) => {
  const item = await AsyncStorage.getItem(key);
  if (typeof item === 'string') {
    return JSON.parse(item);
  }
  return defaultValue;
};

export const setItemToStorage = async (key: string, value: unknown) => {
  await AsyncStorage.setItem(key, JSON.stringify(value));
};
