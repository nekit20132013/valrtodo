module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          '@screens': './src/screens',
          '@components': './src/shared/components',
          '@constants': './src/shared/constants',
          '@helpers': './src/shared/helpers',
          '@hooks': './src/shared/hooks/index',
          '@types': './src/shared/types',
          '@store': './src/store/',
          '@api': './src/api/index',
        },
      },
    ],
  ],
};
